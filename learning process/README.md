# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### What is the Feynman Technique? Paraphrase the video in your own words

Feynman Technique is a learning technique whose central idea revolves around teaching the thing you're learning to someone else, as it works to improve one's understanding of that concept. The video talks about several steps to inculcate this technique in our learning and they are:

1. Choose a concept to learn and write its name on a piece of paper
2. Write your explanation in simple language as if you're teaching it to someone else
3. Return to the source material if you get stuck
4. Challenge yourself to simplify your explanations and create analogies

## What are the different ways to implement this technique in your learning process?

One scenario where I can apply the Feynman Technique that I can think of is scheduling personal presentations, to my colleagues, on topics that I find difficult. That way, I'll have to understand the presentation topic well beforehand and I get an opportunity to teach the topic to my colleagues, to improve my understanding of it. 

While preparing for a presentation I can think of making handwritten notes on the presentation topic on my iPad. This will allow me to write explanations in simple language and if I get stuck, I can go back to the source material and even get some feedback from my colleagues.

I'll also try incorporating analogies and real-life examples in the presentation to improve their understanding and thus mine.

## 2. Learning How to Learn TED talk by Barbara Oakley

### Paraphrase the video in detail in your own words

The video starts by taking a short journey through the life of Barbara Oakley. She described how at a young age, she struggled with math and science in elementary and high school. She describes how she wanted to go to college but because of financial issues, she had to enlist in the military. During her work in the military, she describes how she was inspired to understand all the symbols in math that her counterparts used, to communicate the solution to problems. So after she got out of the military, she was determined to pursue math and science, which she was able to do. She then goes on to describe how she went from struggling with math and science to excelling in them.

The keys to learning effectively. Barbara describes how our brain has two modes of operating; focus mode and diffused mode. When we learn our brain alternates between these two modes. The pinball game analogy follows, where she represents the thought in focused mode as the ball moving through a board where the pins on the board are densely packed. So there is no chance of the ball diverting away from a certain region. She compares the thought in diffused mode to a ball moving through a board where the pins on the board are sparsely located, where there is a chance of the ball getting diverted.

She mentions a technique used by Salvadore Dalí and Thomas Edison, to snap from diffused mode into focused mode.

She then talks about how to stop procrastination using the Pomodoro Technique. Get a timer and set it for 25 minutes and make sure everything is off. Work for those 25 minutes. Just after finishing, do something fun. This is a way of practising being focused while also having fun.

Barbara also talks about how students often mistake their best traits for being the worst, while studying one might not be as quick, but their experience is deeper. There is also a phenomenon called the illusion of competence in learning. In this phenomenon, one might feel that they have studied for 6 hrs about haven't learned anything as they were not using the focused part of their brain.

Finally, Mrs Oakley also touches on how recalling the concepts that one has learned through flashcards, mind maps and tests, is just as important as our being in our focused mode. Understanding alone is not enough unless we are engaging in spaced repetition. We must also try to teach others the concept to understand it better ourselves.

### What are some of the steps that you can take to improve your learning process?

Mrs Oakley described the following techniques to improve one's learning process:

* Spaced Repitition - To remember the concept and fix it into memory. For example, we cannot recall an entire song just by listening to it once. We have to play it on repeat to fix the lyrics in our memory.
* Teaching others - To understand the concept better (Feynman Technique).
* Making Flashcards - To recall the nitty gritties of a concept
* Making mindmaps - To make break down concepts into chunks and mapping how they are related to each other
* Giving Test - To rectify mistakes and refer back to the source content

## 3. Learn Anything in 20 hours

### Your key takeaways from the video? Paraphrase your understanding

Josh starts by talking about the struggle of how to juggle a child and career, while also taking a time for yourself. He debunks the mystery of people requiring 10,000 hours for learning something new by clarifying that it is only true if one wants to reach expert-level performance. We're talking about top level athletes and chess masters. This misinformation was perpetreated by the bestseller books, which popularized this arbitrary myth through grape wine communication which then became the face of requirement for anyone to learn anything.

When we start to learn something new, we require considerable amount of time to get a performant output and it reduces when one practices the skill. Performant time (time required to give performant output) is inversely proportional to practice time.

The learning curve is steeper when one is just starting to learn something new. The curve becomes gentle when we practice the skill over time. As per Josh Kaufman's research, it takes 20 hrs to learn something new. A focused deliberate practice of 20 hrs is enough to get competent at any skill.

The method to utilize those 20 hrs efficiently can be enumerated as:

* Deconstruct the skill: Break the skill and its requirements into smaller pieces and work on the important ones.

* Learn enough to self-correct: Avoid procrastination by doing just the minimum and practice just to make mistakes and to correct them.

* Remove pratice barrries: Removing distraction allows you to activate the focus mode of your brain

* Practice for atleast 20 hours - By precommiting to 20hr practice, one is able to overcome the frustration barrier and stick with pratice to learn it thoroughly.

### What are some of the steps that you can while approaching a new topic?

For ages, I've stuck to some techniques that have really helped me:

* Learning the bare minimum and then solving problems related to the topic.

* Spending some time with a new topic always helps to overcome learning anxiety.

* If I ever get stuck, I go back to the source material. And the joy getting unstuck is immense.

* If I want to get even better or deeper into a topic, I'll try explaining the concept to my colleagues or fiends.
