# Tiny Habits

## Tiny Habits - BJ Fogg

### 1. Your takeaways from the video (Minimum 5 points)

My key takeaways from BJ Fogg's TEDx session:

* Celebrate every accomplishment no matter how trivial it is because it motivates us to do better in future.

* There can be fifteen ways in which behaviour can change. In long term, there are only ways in which behaviour can change. The first is changing the environment which also includes the social environment and the second is performing a familiar behaviour just before performing the habit that you want to cultivate.

* The three things that need to happen at the same time for it to cause behaviour are called MAT. First is the presence of motivation, second is the ability to do it and the last is a trigger. The triggers fail when a high level of motivation is required to perform the behaviour and the task is difficult to perform. The smallest amount of motivation and ability at which this occurs is called the activation threshold.

* So we respect the activation threshold and do tasks that take the least amount of motivation and don't require much effort i.e. breaking down the task into an extremely simple task that is very easy to perform.

* Follow the formula for cultivating new habits, like so

```english
After I <insert a permanent habit>, I will do <insert a temporary habit>.
```

This will anchor your new habit or behaviour to an existing habit, motivating you to cultivate this new habit as a ritual that is tied to the previous habit. The existing habit has to be something that has long been cultivated, it can be something like peeing or brushing.

## Tiny Habits by BJ Fogg - Core Message

### 2. Your takeaways from the video in as much detail as possible

BJ Fogg has discovered the Universal Formula for Human Behavior.

```english
B = MAP, where:
B - Behavior
M - Motivation
A - Ability
P - Prompt
```

Say you want to read a book, what the above formula says is that you'll read a book only if your motivation, ability to read and the prompt to read are present.

The Tiny Habits method asks us to do three things to turn a behaviour into a habit.

1. **Shrink the behaviour:** To perform a behaviour, we need to be above the activation threshold, which means high ability requirements should balance out with low motivational requirements and vice versa. So one way of doing this is finding the tiniest version of the behaviour either by reducing the quantity or by doing the first step so that we require the tiniest amount of motivation. For example, the tiniest version of doing a 60-minute meditation practice would be doing it only for a minute.

2. **Identify an action prompt:** There are three types of habit prompts (things that remind you to do the habit); External prompts (like phone notifications), Internal Prompts (like internal thoughts) and action prompt (like completion of one behaviour that reminds you of another). External and internal prompts are very easy to ignore and they're also disruptive and demotivating. So we should rely more on action prompts i.e. completion of one thing while having the momentum of the completed task can help complete the next one. That momentum has enough motivation to allow you to be above the activation threshold. You can make your action prompt using this format: `After I <insert a permanent_habit>, I will <insert a habit to inculcate>.`

3. **Grow your habit with some shine:** Inculcating a habit is a continuous process, you have to nurture it through appreciation and celebration. BJ Fogg defines shines as the feeling of accomplishment that one gets after completing a habit. He also calls it 'Authentic Pride'. To nurture a habit, you have to generate the same amount of intense feeling of accomplishment that one might get after a job promotion or winning a contest, after executing the habit. Celebrating even small wins increases your motivation, your confidence and also your pride. This celebration also called 'Success Momentum' increases with the frequency of your success and not the size of it.

BJ Fogg's golden words, "Small might not be sexy, but it's successful and sustainable".

### 3. How can you use B = MAP to make making new habits easier?

BJ Fogg stresses on cultivating new habits by starting tiny, as tiny as you can get. This is because out ability to do something is related to our motivation, our ability and a prompt that reminds us to do the thing. For us to do something our motivation and ability should be above the activation threshold, so we have to balance ability with motivation and vice versa. So starting with something small means that we don't require a lot of motivation or ability and we can easily cross the activation threshold. Then to boost our motivation, we have to keep celebrating our accomplishments no matter how tiny. This will also help us keep above the activation threshold.

### 4. Why it is important to "Shine" or Celebrate after each successful completion of habit?

What "Shine", the feeling of accomplishment that one gets after executing a habit, does is to help us boost our motivation. When you give yourself a steady dose of 'Shine' after doing the tiniest version of habit, your motivation steadily grows. When your motivation increases you move higher up the activation threshold and can tackle harder habits easily. Also, without a sense of pride or achievement, we tend to lose our motivation and success momentum which is a requirement for prompt and being above the activation threshold.

## 1% Better Every Day Video

### 5. Your takeaways from the video (Minimum 5 points)

* Habits are a compound interest in self-improvement. What it means is that the little thing that you do compound over time and contribute to your success in accomplishing your long-term goals, Brailsford calls it the aggregation of marginal gains.

* There are four stages of habit transformation; noticing, wanting, doing and liking. Noticing is having a gist of what exactly you want to accomplish. Wanting is the desire to achieve the habit. Doing is executing the habit. Liking comes from celebrating and appreciating the execution of the habit.

* One of the strategies for noticing is called Implementation intentions. It's a preparation of implementing a habit by stating the whens, wheres and hows of executing that habit. This removes the decision-making that is required every time you execute a habit. It also gives your goals a time and a place to live in the world.

* Always have the failure premortem, a statement of all that can go wrong which could lead to failure. This way you make better decisions and avoid failure. These plans are if...then statements i.e. if one particular bad thing happens then do this.

* The wanting part of habit transformation comes from one of the most overlooked drivers of your habit is the physical environment. Our desires are influenced by the environment we are in. In a negative environment, sticking to positive habits is difficult. We can cheat a negative environment once or twice, but not more.

* Any outcome is just a point along the spectrum of repetitions. The more repetitions and efforts that you put in, the more goals you're likely to achieve. Any repetition of the habit is just an exercise of getting started each day. The goal is to optimize the beginning of any task so that it's easier to start executing the task. This optimization acts as an entrance ramp to a bigger routine.

* The only reason we repeat behaviour is that we like them. So we must experience rewards along the way. The best way to change long-term behaviour is short-term feedback.

## Book Summary of Atomic Habits

### 6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

We tend to underestimate the power of making small improvements repetitively over a longer period of time. Real change comes from hundereds of small decisions or habits that accumlate to produce remarkable results. To form atomic habits, what we need is:

1. 1% rule of getting better with time - If we keep getting 1% better every day for 1 year, by the end of the year, we'd get 37 times better by the end of the year. Habits don't seem to make much difference on a given day, but the impact over months or years can be enormous. Slow rate of transformation can also help bad habits creep in. Habits don't seem to matter until we cross some critical threshold. So we might feel like we're not making much progress, but we can overcome it be celebrating even the tiniest of accomplishments.

2. Focusing on systems instead of goals - Achieving a goal is only a momentary change. To sustain the change, we need to change the system itself. Goals restrict our happiness. We think we'll be happy only after we have achieved our goals. Goals are at odd with long-term progress. The pupose of building is to continue winning the game i.e. sustaining the habit.

3. Changing identities rather than outcomes - The ultimate form of motivation is when a habit becomes a part of you identity.

4. The four laws of behaviour change - The four things that allow us to build a habit are: cues, cravings, response and rewards. You want to put fewer steps between you and the good behaviour and more steps between you and the bad one. Being in an environment that exposes you to the cues of your positive habit helps you.

### 7. Write about the book's perspective on how to make a good habit easier?

Start with an incredibly small habit, which doesn't require much motivation. Increase your habit in very small ways, so that we have enough time that it becomes ingrained in us and requies little to no motivation at all. Break habits into chunks, so that we're below the activation threshold. Make sure that you require the minimum amount of effort and steps to complete the task.

### 8. Write about the book's perspective on making a bad habit more difficult?

Triggers are the first step in developing a habit, so identify all the triggers. Put as many step as possible before the execution of a bad habit, so that it require more motivation and effort to perform it. Once you understand the situation that triggers your habit and the reward you receive for engaging in the undesirable behavior, you can make a plan that involves goals for behavior change and strategies for minimizing habit triggers.

## Reflection

### 9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

One habit that I would like to do more of is exercising. So I think of getting exercise by playing table tennis in the office, that way, I don't require much motivation to do exercise indirectly and I'm always above the activation threshold. Playing table tennis can be rewarding and fun. I also make sure I play everyday at the same time, so time is a prompt that reminds me to play. Defeating other players is satisfying. I always celebrate my wins, even if it is against a novice player.

### 10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

A habit that I'd like to eliminate is of checking my phone regularly, even during work. I'd try removing the prompt like notification, which can be silenced by putting the phone in do not disturb mode. The other technique that I find useful is keeping my phone face down on my desk so that I'm less tempted to pick it up because then I won't see any screen flash caused by a new social media notification. The other thing I've found helpful is keeping my phone inside my bag, which doesn't remind me of social media at all. I also keep it deep inside my bag so that the process of taking it out takes one more step. I also find, hiding all social media apps in folders a bit helpful. Now it takes two or three steps more to get to them.
