# Learning and Active Communication

## Active Listening

### What are the steps/strategries to do Active Listening?

Enumerated are a few strategies to engage in active listening:

1. Avoid getting distracted by your thoughts.

2. Focus on the speaker and the subject being discussed.

3. Don't interrupt the person who is speaking.

4. Respond only after the speaker has finished talking.

5. Use door openers, phrases that show that the listener is interested in what the speaker has to say.

6. Adjust your body language to show that you're interested in listening to what the speaker has to say.

7. Takes notes during an important conversation, if you deem it appropriate.

## Reflective Listening

### According to Fisher's model, what are the key points of Reflective Listening?

The key takeaways from Dalmar Fisher's model are as follows:

1. Listen more than talking

2. Respond to what is personal in the conversation rather than what is distant or abstract.

3. Restate and clarify what the other party has said and don't ask questions or express your feelings, believes or wants when you're listening.

4. Try to understand the feelings contained in the speaker's voice and not just the facts or ideas from their speech.

5. Respond with acceptance and empathy and not with indifference, cold objectivity or fake concern.

6. Avoid stereotyped reactions like repeating the phrase "you feel that ..." or "you're saying that ..."

## Reflection

### What are the obstacles in your listening process?

Personally, one of the main obstacles that I face is not being able to hear what the other party is speaking. This can lead to many problem from misunderstanding to missing out important details in the conversation. Other, being rare, is getting distracted by my own thoughts and zoning out during a conversation. This can also lead to problem of misunderstaning and losing details. Anxiety is one of the other barriers that arise from competing personal worries and concerns. Some of the external barrier in listening are noise and visual distractions.

### What can you do to improve your listening?

Improving listening involves removing external and internal barriers. External barriers to listening are easier to manage. So problems like noise and visual distractions can be overcome by moving the conversation to a different place. Physically facing the speaker and attempting to make frequent eye contact might also aid in listening and avoiding.

Bonding with your speaker shows them that you're engaged. One may be eager to share their personal experience when listening, a better approach typically involves listening and providing responses that focus on the other person's situation, which shows that you're genuinely invested in their side of the conversation.

## Types of Communication

### When do you switch to Passive Communication style in your day to day life?

I switch to passive communication when the conversation in a group revolves around a topic about which I have less knowledge. I can only form an opinion about something by first studying it or reading about it. Although it might not be an effective form of communication, it is better to speak when you have complete knowledge instead of having little knowledge or no knowledge at all.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

I usually use sarcasm and taunts only when I'm having converstion with my close friend, who aren't colleagues, as I understand others, who don't know me well, might take offense at sarcasm and taunts. Gossping is also something I restrict to my close friend circle and avoid it with my colleagues. This style of communication should always be restricted to your inner circle and void such tone with your coworkers or in offices.

### How can you make your communication assertive? You can analyse the video and then think what steps you can apply in your own life?

Using statements that start with 'I', lets others know that what I'm really thinking without sound accusatory. A simple change from "You're wrong" to "I disagree" makes a huge difference in being assertive.
Being direct and saying no, as another way of being assertive. Its a great reminder that no is a complete sentence and we don't need to exlain why we choose to say no. Also if explaination is appropriate, keep it brief.
Acting confident having and upright posture while leaning towards the speaker can also imply assertiveness. Having a periodic time contact communicates that the person is not intimated.
