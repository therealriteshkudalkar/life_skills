# Grit and Growth Mindset

## Grit

### 1. Paraphrase the video in a few lines

Angela Lee Duckworth stresses how we need in education, a much better understanding of students and learning from a motivational and psychological perspective. Mrs Duckworth also talks about how IQ isn't the only parameter to gauge students' performance in class.

One trait that was common in all the consistent best performers was grit. Grit can be defined as passion and perseverance for very long-term goals. In a personal study conducted by Mrs Duckworth where she supplied a grit questionnaire to her students and waited a year to see who would graduate, she found out the grittier students were more likely to graduate and this was irrespective of family income, safety, etc. Grit is unrelated to talent.

Grit can be inculcated through a growth mindset. A growth mindset is a belief that the ability to learn is not fixed and can change with effort. When kids learn about how challenges alter their brain, they are more likely to persevere when they fail because of their belief that failure is not a permanent condition.

### 2. What are your key takeaways from the video to take action on?

It's not IQ that matter to succeed in life, but grit and perseverance. Excelling at everthing is great, but when we have a setback, we need to overcome it, that is more valuable to succeed in life. Grit can be inculcated through growth mindset. The belief that failure is not permanent is a great way to approach any setback.

## Introduction to Growth Mindset

### 3. Paraphrase the video in few line in your own words

A growth Mindset is a belief in your capacity to learn and grow. This idea was pushed by professor Carol Dweck. For decades, Carol Dweck has been studying why some
people succeed while people, who are equally talented, do not and her studies have found that it people's mindsets play a crucial role in this process.

There are two ways in which people think about learning; a fixed mindset and a growth mindset. The areas where these two differ are beliefs and focus. People having a fixed mindset believe that intelligence is set and that you either have them or you don't. They also believe that some people are naturally good at things, while others are not i.e. people do not have control over their abilities. While people having a growth mindset believe that skills and intelligence are grown and developed. People who are good at something are good at it. After all, they built that ability and people who aren't, are not good because they haven't done the work i.e. people are in control of their abilities.

A growth mindset is the foundation of learning. The key ingredients for growth are effort, challenges, mistakes and feedback. People with fixed mindset view effort as something not necessary or useful, they also back down and avoid challenging situations. They get discouraged when they make mistakes. When they received feedback, they tend to get defensive instead of treating it as something that can be improved upon. People with a growth mindset view effort as useful or something that leads to growth. They embrace challenges and persevere and frame mistakes as an opportunity to learn and grow. When they receive feedback, they appreciate it and use it.

### 4. What are your key takeaways from the video to take action on?

For perseverance, having a growth mindset is essential. People with a growth mindset take challenges head-on. They try and fail, but they also get up again and they learn from their mistakes. Getting discouraged by setbacks is not a thing with people who have inculcated a growth mindset. It's one of the ways to be successful in life. It's a valuable trait to have. It's also essential that we don't see feedback as an insult but as an opportunity to learn and grow.

## Understanding Internal Locus of Control

### 5. What is the Internal Locus of Control? What is the key point in the video?

Internal locus of control can be defined as the degree to which one believes that they have control over their life. The people who are led to believe that they are smart and gifted are led to believe in external locus of control i.e. factors outside of what they could control were the reason they did well. The people who are led to believe that they did well because of hard work were led to believe in internal locus of control i.e. factors they control were the reason they did well.

Having an internal locus of control is key to staying motivated. One must have a sense of responsibility in their life, what decisions they take affect not only their life but also other people's life. Having such a sense is essential in staying motivated. When things go wrong, people having an external locus of control blame it on external stuff instead of blaming themselves.

Solving real-life problems in our own life and appreciating the fact that it was your actions that solved this problem is essential for developing an internal locus of control.

## How to build a growth mindset?

### 6. Paraphrase the video in a few line in your own words

Two of the most fundamental stuff required to develop a growth mindset are self-belief and questioning. The belief that one can figure out how things work and that one can get better is essential for having a growth mindset. People lacking this belief don't have what psychologist call expectancy or agency. The second most important trait is one's ability to question assumptions.

Don't let your knowledge skills and ability narrow down your self-expectations and your vision for the future. What one is capable of doing today has little or nothing to do with what you're capable of or achieve in the future. So don't let the current assumptions of your capabilities hold you back, always question them.

Be the architect of your dream and make your life curriculum. Search for something that you're passionate about. Forge your path towards the goals you want to achieve. You will certainly face setbacks and discouragement, but don't give up and keep learning, also honour the struggle. Because the struggle is a part of the learning journey and it strengthens you. That is how you develop a long-term growth mindset.

### 7. What are your key takeaways from the video to take action on?

Developing a growth mindset requires two important thing and those are self-belief of being capable enough to do any given task and question your own assumptions about yourself. Don't let your current knowlege and assumption hold you back as it little to no correlation with your capability to achieve something in the future. Also one will always face setbacks but with correct attitude, we can even overcome these and always honor these struggles. They're what makes you, you!

## Mindset - A MountBlue Warrior Reference Manual

### 8. What are one or more points that you want to take action on from the manual?

I'd like to improve on point 14 which talks about being focused until I achieve mastery and can do things half asleep. To achieve this, I'll put in more effort to learn and make sure I remember the concept and peculiar details.

I'd also like to take action on point 11. Wearing and displaying confidence. I know I'm capable of doing stuff and that capability I'm going to show is through confidence.
