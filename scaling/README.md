# Scaling 

When demand for an application is soaring, a developer quickly recognize the need to maintain an app’s accessibility, uptime, and capacity in the face of increased load. <sup>[1](#references)</sup>

If you are running a website, web service, or application, its success hinges on the amount of network traffic it receives. It is common to underestimate just how much traffic your system will incur, especially in the early stages. This could result in a crashed server and/or a decline in your service quality. <sup>[2](#references)</sup>

Scalability describes your system’s ability to adapt to change and demand. Good scalability protects you from future downtime and ensures the quality of your service. <sup>[2](#references)</sup>

Systems have four general areas that scalability can apply to:

- CPU
- Disk I/O
- Memory
- Network I/O

There are two strategies to target the problem of scaling; __*horizontal scaling and vertical scaling*__.

![Vertial and Horizontal Scaling](https://www.cloudzero.com/hubfs/blog/horizontal-vs-vertical-scaling.webp "Vertical and Horizontal Scaling")

A useful analogy for understanding this distinction is to think about scaling as if it were upgrading your car. Vertical scaling is like retiring your Toyota and buying a Ferrari when you need more horsepower. With your super-fast car, you can  zoom around at high speed with the windows down and look amazing. But, while Ferraris are awesome, they’re not very practical- they’re expensive, and at the end of the day, they can only take you so far before they’re out of gas (not to mention, only two seats!). <sup>[1](#references)</sup>

Horizontal scaling works similarly in that it gets you that added horsepower, but it doesn’t mean ditching the Toyota for the Ferrari. Instead, it’s like adding another vehicle to a fleet. You can think of horizontal scaling like several vehicles you can drive all at once. Maybe none of these machines is a Ferrari, but no one of them needs to be: across the fleet, you have all the horsepower you need. <sup>[1](#references)</sup>

## Vertical Scaling

Through vertical scaling (scaling up or down), you can increase or decrease the capacity of existing services/instances by upgrading the memory (RAM), storage, or processing power (CPU). Usually, this means that the expansion has an upper limit based on the capacity of the server or machine being expanded.  <sup>[3](#references)</sup>

### Advantages

- Cost-effective - Upgrading a pre-existing server costs less than purchasing a new one.
- Less complex process communication - When a single node handles all the layers of your services, it will not have to synchronize and communicate with other machines to work.
- Less complicated maintenance - Not only is maintenance cheaper but it is less complex because of the number of nodes you will need to manage.
- Less need for software changes - You are less likely to change how the software on a server works or how it is implemented.

### Disadvantages

- Higher possibility for downtime - Unless you have a backup server that can handle operations and requests, you will need some considerable downtime to upgrade your machine.
- Single point of failure - Having all your operations on a single server increases the risk of losing all your data if a hardware or software failure was to occur.
- Upgrade limitations - There is a limitation to how much you can upgrade a machine. Every machine has its threshold for RAM, storage, and processing power.

## Horizontal Scaling

To scale horizontally (scaling in or out), you add more resources like virtual machines to your system to spread out the workload across them. Horizontal scaling is especially important for companies that need high availability services with a requirement for minimal downtime. <sup>[3](#references)</sup>

### Advantages

- Easy to resize according to your needs
- Immediate and continuous availability
- Cost can be linked to usage and you don’t always have to pay for peak demand

### Disadvantages

- Architecture design and deployment can be very complicated
- A limited amount of software that can take advantage of horizontal scaling

## Load Balancing

Load balancing lets you evenly distribute network traffic to prevent failure caused by overloading a particular resource. This strategy improves the performance and availability of applications, websites, databases, and other computing resources. It also helps process user requests quickly and accurately. <sup>[4](#references)</sup>

If a single server handles too much traffic, it could underperform or ultimately crash. By routing user requests evenly across a group of servers, load balancers minimize the likelihood of downtime. <sup>[4](#references)</sup>

Load balancing performs these critical tasks:

- Manages traffic spikes and prevents spikes on a single server.
- Minimizes user request response time.
- Ensures performance and reliability of computing resources, both physical and virtual.
- Adds redundancy and resilience to computing environments. <sup>[4](#references)</sup>

### Hardware Load Balancing

Hardware load balancers consist of physical hardware, such as an appliance. These direct traffic to servers based on criteria like the number of existing connections to a server, processor utilization, and server performance. <sup>[4](#references)</sup>


### Software Load Balancing

Software load balancers can come in the form of prepackaged virtual machines (VMs). VMs will spare you some of the configuration work but may not offer all of the features available with hardware versions. <sup>[4](#references)</sup>

### Load Balancing Algorithms

To do their work, load balancers use algorithms—or mathematical formulas—to make decisions on which server receives each request. <sup>[4](#references)</sup>

Load balancing algorithms fall into two main categories—weighted and non-weighted. <sup>[4](#references)</sup>

__*Weighted algorithms*__ use a calculation based on weight, or preference, to make the decision (e.g., servers with more weight receive more traffic). The algorithm takes into account not only the weight of each server but also the cumulative weight of all the servers in the group. <sup>[4](#references)</sup>

- Weighted Round Robin
- Least Connections
- IP Hash
- Least Time

__*Non-weighted*__ algorithms make no such distinctions, instead of assuming that all servers have the same capacity. This approach speeds up the load balancing process but it makes no accommodation for servers with different levels of capacity. As a result, non-weighted algorithms cannot optimize server capacity. <sup>[4](#references)</sup>

- Round Robin
- Sticky Session
- Random with Two Choices

## References

1. [Think About Long-Term Viability](https://www.missioncloud.com/blog/horizontal-vs-vertical-scaling-which-is-right-for-your-app)

2. [Horizontal Vs. Vertical Scaling: How Do They Compare?](https://www.cloudzero.com/blog/horizontal-vs-vertical-scaling)


3. [Scalability in Cloud Computing: Horizontal vs. Vertical Scaling](https://www.stormit.cloud/blog/scalability-in-cloud-computing-horizontal-vs-vertical-scaling/)


4. [Load Balancing](https://www.ibm.com/cloud/learn/load-balancing)


5. [What Is Load Balancing?](https://www.nginx.com/resources/glossary/load-balancing/)
