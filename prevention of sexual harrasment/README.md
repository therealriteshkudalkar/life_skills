# Prevention of Sexual Harrassment

## What kinds of behaviour cause sexual harassment?

Any behaviour that is unwanted, improper or offensive in the work place may be deemed as sexual harrassment. They may be categorized as follows (but it is not limited to this):

1) Quid Pro Quo
   * Distributing unwanted jokes, images or symbols that others may find offensive
   * Demand or request for sexual favours
   * Threaten about employment

2) Hostile work environment
   * Discriminatory attitude
   * Subjecting to prejudice
   * Stalking colleagues
   * Inappropriate and unsolicited physical contact
   * Cat calling
   * Being persistent about certain work unrelated demand
   * Humiliating and bullying colleagues

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

If I witness or face sexual harrassment at work place, I'd first warn the perpetrator to back off and ask them not to engage in such acts. If the person continues to engage in same behaviour again or threatens me or my colleagues about work, then I'd report it to the Department of Human Resource Management.

## Explains different scenarios enacted by actors

Some of the scenarios enacted by the actors are:

* The joke's on you - Here a guy sends a seemingly harmless joke, which others may or may not find offensive, to all his colleague. But some colleagues are not pleased, as they find the images of the joke to be offensive. Here the take away is that what we might percive to be inoffensive may not necessarily be true for other people. Hence it is wise to be careful about the articles or jokes we send to our colleagues.

* The legend - Here a guy, who has this habbit of hugging other people, tries hugging a colleague who had warned the guy that their hug wasn't going to make their day. This was an unsolicited physical contact, which others may or may not find offensive. He was also warned about it, but still did it, making his colleague uncomfortable, creating a hostile workplace environment. Take away is that one should avoid unsolicited physical contact at any cost.

## How to handle cases of harassment?

It is always better to first warn the perpetrator that the behaviour they are engaging in is sexual harrasment. If they don't stop, then one must escalate the situtation to Human Resource Management.

## How to behave appropriately?

One should also we aware that what they find inoffensive might not be the case when it comes to other people. It is always better to avoid jokes or memes that one might not express in front of their family, children or the ones that are not safe for work. One should avoid passing a comment on their colleagues, especially if its about their body. Racism, homophobia, transphobia, casteism and misogyny are strongly discouraged in work places.
